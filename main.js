var express = require('express')
var app = express();
//app.use('/',  express.static('./web'));

app.use('/', express.static(__dirname + '/'));
app.set('views', __dirname + '/app/');
app.set('view options', { layout:false });

app.get('/', function (req, res) {
  res.render('index.html');
});

/*app.get('/i', function (req, res) {
  res.render('index2.ejs');
});

app.get('/getImages/:user/:minPosition/:limitList', API.Twitter.getImagesByUser);
*/


app.listen(3001);