
//los tags no se pueden repetir ni tener menos de 3 caracteres
function selectTags(title) {
    var tags = [];
    var temp = title.split(' ');

    for (var i = 0; i < temp.length; i++) { 
        if(temp[i].length > 3){
            if(tags[temp[i]] === undefined)
            {
                tags.push(temp[i]);
            }
        }
    }
    return tags;
}
    
chrome.browserAction.onClicked.addListener(function (tab) {

    var items = [];
    chrome.history.search({text: '', maxResults : 500}, function(data) {
        data.forEach(function(page, index) {
            var item = {
                    chormeId :            page.id,
                    url:            page.url,
                    title:          page.title,
                    lastVisitTime:  page.lastVisitTime,
                    visitCount:     page.visitCount,
                    typedCount:     page.typedCount,
                    tags:           selectTags(page.title),
                    visitItems : [] ,
            }
            chrome.history.getVisits({ "url": item.url},
                function(visitItem) {
                    visitItem.forEach(function(visit){
                        if(visit){
                            var vi = {
                                id : visit.id,
                                referringVisitId:   visit.referringVisitId,
                                transition: visit.transition,
                                visitId: visit.visitId,
                                visitTime: visit.visitTime,
                            }
                            item.visitItems.push(vi);
                        }
                    });
                    
                    if(data.length == index+1){
                        onComplete(items);
                    }
            });
            items.push(item);
            
        });
    });
    
    function onComplete(items){
        var all = {  items: items };
        var xhr = new XMLHttpRequest();
        //xhr.open("POST", "http://dev-linksly.herokuapp.com/note/saveNoteWithLinks/");
        xhr.open("POST", "http://localhost:3000/v.1/chromeHistory");
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = function () {
            console.log(xhr.responseText)

            if (xhr.readyState == 4 && xhr.status == 200) {
                alert(xhr.responseText);
            }
        }
        var js = JSON.stringify(all);
        xhr.send(js);
       // console.log (js);
    }

});

