
var App = {
    Security : {
        token : '',
        user : '',
        mail:'',
        lastAddHistory : '',
        loginIN : false,
        login   : function(loginOK) {
            return new Promise(function(resolve, reject){
                loginOK = resolve;
            });
        },
    },
    Common : {},
    Providers: {},
    Directives: {},
    Views: {},
    Services: {},
    Controllers: {},

    init: function () {  
        
        var linkslyApp = angular.module('linkslyApp',['ngRoute','lumx','wu.masonry','ngMaterial', 'ngMessages']); //, 'ui.scroll']);

//        angular.module("linkslyApp").value("THROTTLE_MILLISECONDS", 250);

        linkslyApp.factory('AuthenticationService', ['$http',  App.Services.Authentication]);
        linkslyApp.factory('HistoryService', ['$http', '$q',  App.Services.History]);
        linkslyApp.factory('FlashService', ['$rootScope',  App.Services.Flash]);
        
        linkslyApp.factory('TabsService', ['$rootScope', '$http', '$q','HistoryService', 'SocketService','CloudinaryService', App.Services.Tabs]);
        linkslyApp.factory('CloudinaryService', ['$q', App.Services.Cloudinary]);
        linkslyApp.factory('SocketService', App.Services.Socket);
        linkslyApp.factory('util', App.Common.Utils);
        
        linkslyApp.controller('LoginController', ['AuthenticationService',
            '$scope', '$rootScope','$location','FlashService','SocketService','LxNotificationService', App.Controllers.Login]);
        
        linkslyApp.controller('HomeController',
         [ 'SocketService', 'AuthenticationService','TabsService', '$scope', '$rootScope', '$http','$timeout', '$q','util',App.Controllers.Home2]);        
        linkslyApp.config(function( $routeProvider, $locationProvider, $httpProvider,$compileProvider) {
            
            //para visualizar las img fijas
            $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|http|chrome-extension|filesystem):/);
    
            $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
            $routeProvider
                .when('/', {
                    controller: 'HomeController',
                    templateUrl: 'app/views/home2.html',
                    controllerAs: 'vm'
                })

                .when('/login', {
                    controller: 'LoginController',
                    templateUrl: 'app/views/login.html',
                    controllerAs: 'vm'
                })

                /*.when('/register', {
                    controller: 'RegisterController',
                    templateUrl: 'register/register.view.html',
                    controllerAs: 'vm' 27135125488
                })*/

                .otherwise({ redirectTo: '/login' });
        });
        linkslyApp.run(function ($rootScope, $location, $http,SocketService) {
            if (App.Security.token) {
                $http.defaults.headers.common['x-access-token'] =  App.Security.token;
            }
            
           
            $rootScope.$on('$locationChangeStart', function (event, next, current) {
             // redirect to login page if not logged in and trying to access a restricted page
                var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
                var loggedIn = App.Security.user;
              
                if (restrictedPage && !loggedIn) {
                    $location.path('/login');
                }
             });
         });
    }
};
//App.init();