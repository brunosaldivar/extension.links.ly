
'use strict';
App.Services.Tabs = function($rootScope, $http, $q, HistoryService,SocketService, CloudinaryService)
{
	var tabService = {};
	
	tabService.data  = [];


   tabService.getVisits = function (url){
        //console.log("iniciando getVisits..")
        return $q(function(resolve, reject){
            chrome.history.getVisits({url: url}, function(visitItems) {
                if(chrome.runtime.lastError) {
                    reject(chrome.runtime.lastError.message);
                } else {
                    
                    resolve(visitItems);
                }
            });
        });
    }
    tabService.getHistory = function (tabInfo){
        //console.log("iniciando getHistory..")
        return new $q(function(resolve, reject){
            if(App.Security.lastTimeHistory){
                chrome.history.search(
                    {'text': tabInfo.url,  
                    'maxResults': 10,
                    'startTime': App.Security.lastTimeHistory
                //	'startTime': startTime => por defecto un dia antes => lo bajo de la config, la ultima vez que me conecte
                }, function(historyItems) {
                        if(chrome.runtime.lastError) {
                            reject(chrome.runtime.lastError.message);
                        } else {
                            resolve(historyItems);
                        }
                    });
            }else{
                chrome.history.search(
                    {'text': tabInfo.url,  
                    'maxResults': 10
                }, function(historyItems) {
                        if(chrome.runtime.lastError) {
                            reject(chrome.runtime.lastError.message);
                        } else {
                            resolve(historyItems);
                        }
                });
            }
        });
    }
    tabService.captureVisibleTab = function( tabInfo){
        //console.log("iniciando captureVisibleTab..");
        return new $q(function(resolve, reject){

            chrome.tabs.captureVisibleTab(tabInfo.windowId, function(img) {
                if (chrome.runtime.lastError){
                    console.log('Error selecting tab: ' +  chrome.runtime.lastError.message);
                    reject (chrome.runtime.lastError.message);
                }
                else{
                    
                    CloudinaryService.upload(img, tabInfo).then(function(data){
                        //console.log('enviando: ' +  tabInfo)
                        resolve(data)
                        
                    });
                }
            
            });
        });
    }
    tabService.getTabInfo    =  function()
    {
        var myWindowId = 0;
        chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {   
            if(tab.title == 'Links.ly Chrome Extension') { myWindowId = tab.windowId; return; }
            //comprobar que no se repita 
            if( tab.status == 'complete' &&  tabService.data.indexOf(tab.url) === -1  ){

                if(tabService.data[tab.url]) {console.log("URL existente en array"); return;};

                var tabInfo = {};
                tabInfo.favIconUrl = tab.favIconUrl;
                tabInfo.title = tab.title;
                tabInfo.url = tab.url;
                tabInfo.tags = App.Commons.getTags(tab.title);
                tabInfo.windowId = tab.windowId;
                tabInfo.date    = Date.now();
                
                // 1 - Traer historial
                // 2 - Traer visitas
                // 3 - mandarla al servicio de img -> si sale mal se manda igual hasta aca
                //$q.all([

                    //tabService.getHistory(tabInfo),
                    //tabService.getVisits(tabInfo.url)

                //]).then(function(data) {
                    console.log(data)
                    tabService.captureVisibleTab(tabInfo).then(function(imgData){

                    }).catch(function(err){

                    });
                 /*   tabInfo = data[0];
                    historyItems = data[1];
                    visitItems = data[2];*/
                // }).catch(function(err){ 
                //     console.log(err); 

                // });
                tabService.data.push(tab.url);
                $rootScope.$emit("tabUpdate", tabInfo);

            }
        });
    }
  
    return tabService;
}