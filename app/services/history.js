App.Services.History = function($http, $q)
{
	var historyService = {};
	
	historyService.data  = [];
	function selectTags(title) {
		var tags = [];
		var temp = title.split(' ');

		for (var i = 0; i < temp.length; i++) { 
			if(temp[i].length > 3){
				if(tags[temp[i]] === undefined)
				{
					tags.push(temp[i]);
				}
			}
		}
		return tags;
	}
		
		//cambiar  App.Security.lastAddHistory por la info q se baja del user cdo se conecta
	function historyLastWeek() {
		console.log(App.Security.lastAddHistory);
		
		var startTime = 0; 
		if(App.Security.lastAddHistory){

			var myDate=App.Security.lastAddHistory.split("-");
			var newDate=myDate[1]+"/"+myDate[0]+"/"+myDate[2];
			startTime = new Date(newDate).getTime();
		}

		return new $q(function(resolve, reject) {

			chrome.history.search(
        	{'text': '',  
        	'maxResults': 999999999, 
        	'startTime': startTime }, function(historyItems) {
				if(chrome.runtime.lastError) {
					reject(chrome.runtime.lastError.message);
				} else {
					resolve(historyItems);
				}
			});
		});
	}

	function getVisits(historyItem) {

		return new $q(function(resolve, reject) {
			chrome.history.getVisits({url: historyItem.url}, function(visitItems) {
			if(chrome.runtime.lastError) {
				reject(chrome.runtime.lastError.message);
			} else {
				 ///hasta aca llega
				
				resolve({
					historyItem: historyItem, 
					visitItems: visitItems
				});
			}
			});
		});
	}

	historyService.get = function(){
		historyService.data= [];
		return historyLastWeek().then(function(historyItems) {
			return historyItems.reduce(function(sequence, historyItem) {
				
				return sequence.then(function() {
					return getVisits(historyItem);
				}).then(function(result) {
					var page = result.historyItem ;
					var urlItem = {
							chormeId :            page.id,
							url:            page.url,
							title:          page.title,
							lastVisitTime:  page.lastVisitTime,
							visitCount:     page.visitCount,
							typedCount:     page.typedCount,
							tags:           selectTags(page.title),
							visitItems : [] ,
					};

					result.visitItems.forEach(function(visit){
                            var vi = {
                                chromeId : visit.id,
                                referringVisitId:   visit.referringVisitId,
                                transition: visit.transition,
                                visitId: visit.visitId,
                                visitTime: visit.visitTime,
                            };
                            urlItem.visitItems.push(vi);
                        });
					historyService.data.push(urlItem);
				// here, result.historyItem is the history item,
				//   and result.visitItems is an array of visit items
				/* here goes your code #1 */
					/*result.visitItems.each(function(visitItem) {
					 here goes your "Do something that depends on the first function" code 
					});*/
				/* here goes your code #2 */
				// Gotta return some promise
				                  
					$q.resolve(historyItems);
				});
			}, $q.resolve(historyItems));
			
		}).then(function(e) {
			console.log('asfsad')
				 return $q.resolve(historyItems);
			/* here goes your code #3 */
			}).catch(function(errorMsg) {
			// oh noes
			});
		
	}
	historyService.getByURL = function (url) {


		return new $q(function(resolve, reject) {

			chrome.history.search(
        	{'text': url,  
        	'maxResults': 10
        //	'startTime': startTime => por defecto un dia antes
		}, function(historyItems) {
				if(chrome.runtime.lastError) {
					reject(chrome.runtime.lastError.message);
				} else {
					resolve(historyItems);
				}
			});
		});
	}

	return 	historyService;
}
