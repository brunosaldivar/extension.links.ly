'use strict';
App.Services.Socket = function($rootScope) {

    var socketService = {};
    //socketService.socket = {};
    
    socketService.connect = function(token, user){
      
      socketService.socket = io.connect('http://localhost:3000', {query: {token: token, user: { name: user, id : App.Security.user} }});
      socketService.socket.on('connected', function (c) {
          console.log('connected: ');
          console.log(c);
      });
            
      socketService.socket.on('unauthorized', function (st) {
          console.log('unauthorized event ' + st);
          console.log(st);
      });
    }; 

    socketService.on =  function (eventName, callback) {
        socketService.socket.on(eventName, function () {  
          var args = arguments;
          $rootScope.$apply(function () {
            callback.apply(socketService.socket, args);
          });
        });
    };
 
    socketService.emit = function (eventName, data, callback) {
        socketService.socket.emit(eventName, data, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            if (callback) {
              callback.apply(socketService.socket, args);
            }
          });
        });
    };

    return socketService;
 }