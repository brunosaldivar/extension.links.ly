'use strict';
App.Services.Authentication = function( $http)
{
	var authenticationService = {};
	
	authenticationService.data = {
            imgList : [],
            hasMoreItems : false,
            error : {
                  code : null,
                  msg : '',
            },
     };
    authenticationService.login  = function (email, password, callback){

        return	$http.post( 'http://localhost:3000/login',  {'username' : email, 'password' : password}).
            then(function successCallback(response) {

                if(response.data && response.data.token){
                    App.Security.token = response.data.token;
                    App.Security.user =  response.data.user;
                    App.Security.mail =  email;
                    App.Security.lastAddHistory =  response.data.lastAddHistory;
                }
                //console.log(App.Security)
                callback(null, response);
            }, function errorCallback(response) {
                callback(response, null);
            });

        
    }

    return authenticationService;        
}
