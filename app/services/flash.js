App.Services.Flash = function( $rootScope)
{
    var service = this;


    service.initService = function () {
        $rootScope.$on('$locationChangeStart', function () {
            service.clearFlashMessage();
        });

    service.clearFlashMessage = function() {
        var flash = $rootScope.flash;
        if (flash) {
                if (!flash.keepAfterLocationChange) {
                    delete $rootScope.flash;
                } else {
                    // only keep for a single location change
                    flash.keepAfterLocationChange = false;
                }
            }
        }
    }

    service.Success = function(message, keepAfterLocationChange) {
        $rootScope.flash = {
            message: message,
            type: 'success', 
            keepAfterLocationChange: keepAfterLocationChange
        };
    }

    service.Error = function (message, keepAfterLocationChange) {
        $rootScope.flash = {
            message: message,
            type: 'error',
            keepAfterLocationChange: keepAfterLocationChange
        };
    }

    this.initService();

    return service;
}
