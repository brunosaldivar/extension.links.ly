
'use strict';
App.Services.Tabs = function($rootScope, $http, $q, HistoryService,SocketService, CloudinaryService)
{
	var tabService = {};
	
	tabService.tabs  = [];


   tabService.getVisits = function (url){
        //console.log("iniciando getVisits..")
        return $q(function(resolve, reject){
            chrome.history.getVisits({url: url}, function(visitItems) {
                if(chrome.runtime.lastError) {
                    reject(chrome.runtime.lastError.message);
                } else {
                    
                    resolve(visitItems);
                }
            });
        });
    }
    tabService.getHistory = function (tabInfo){
        //console.log("iniciando getHistory..")
        return new $q(function(resolve, reject){
            if(App.Security.lastTimeHistory){
                chrome.history.search(
                    {'text': tabInfo.url,  
                    'maxResults': 10,
                    'startTime': App.Security.lastTimeHistory
                //	'startTime': startTime => por defecto un dia antes => lo bajo de la config, la ultima vez que me conecte
                }, function(historyItems) {
                        if(chrome.runtime.lastError) {
                            reject(chrome.runtime.lastError.message);
                        } else {
                            resolve(historyItems);
                        }
                    });
            }else{
                chrome.history.search(
                    {'text': tabInfo.url,  
                    'maxResults': 10
                }, function(historyItems) {
                        if(chrome.runtime.lastError) {
                            reject(chrome.runtime.lastError.message);
                        } else {
                            resolve(historyItems);
                        }
                });
            }
        });
    }
    tabService.captureVisibleTab = function(tabInfo){
        //console.log("iniciando captureVisibleTab..");
        return new $q(function(resolve, reject){

            chrome.tabs.captureVisibleTab(tabInfo.windowId, function(img) {
                if (chrome.runtime.lastError){
                    console.log('Error selecting tab: ' +  chrome.runtime.lastError.message);
                    reject (chrome.runtime.lastError.message);
                }
                else{
                    
                    CloudinaryService.upload(img, tabInfo).then(function(data){
                        //console.log('enviando: ' +  tabInfo)
                        resolve(data)
                        
                    });
                }
            
            });
        });
    }
    
    tabService.getAll = function(opt){
        var d = $q.defer();
        SocketService.on('tabs.getAllPaging', function(data){
            d.resolve(data.data)
        });
        SocketService.emit('tabs.getAllPaging',opt); 
        

        return d.promise;
    }
  tabService.getAllCount = function(callback){
        var d = $q.defer();
        SocketService.emit('tabs.getAllCount');
        SocketService.on('tabs.getAllCount', function(data){
            d.resolve(data.count);
        });
        return d.promise;
    }

    tabService.lastTab = undefined;
    tabService.getLastTab = function (){
        return this.lastTab;
    };
    tabService.setLastTab = function (value){
        this.lastTab = value;
    };
    tabService.isLastTab = function (url){
        if(this.lastTab){
            return this.lastTab.url == url;
        } 
        return false;
    };
    tabService.getTabInfo  =  function()
    {
        var myWindowId = 0;
        chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {   
            if(tab.title == 'Links.ly Chrome Extension' || tab.title == 'Nueva pestaña'  ) { myWindowId = tab.windowId; return; }
            //comprobar que no se repita 
            if( tab.status === 'complete'  && !tabService.isLastTab(tab.url)){
                var repeat = tabService.tabs.map(function(e) { return e.url; }).indexOf(tab.url) ;
                if( repeat === -1 ){
                    //if(tabService.tabs[tab.url]) {console.log("URL existente en array"); return;}
                    var tabInfo = {};
                    tabInfo.favIconUrl = tab.favIconUrl;
                    tabInfo.title = tab.title;
                    tabInfo.url = tab.url;

                    chrome.runtime.sendMessage(
                        "hlhofdpnopgkheppnfkhamjklodihjfj", 
                        tab.url,
                        function (response) {
                            console.log(response);
                        }
                    );
                    tab.loadingTag = true;

                    //tabInfo.tags = App.Commons.getTags(tab.title);
                    tabInfo.windowId = tab.windowId;    
                    tabInfo.date    = Date.now();

                    tabService.setLastTab(tab);
                    /*tabService.captureVisibleTab(tabInfo).then(function(imgData){
                        f
                    }).catch(function(err){
                        console.log(tabInfo.url + ' error!');
                        console.log(err);
                    }).finally(function(e,d){    */
                        SocketService.emit('tabs.update',tabInfo,function(d){
                            tabService.tabs.push(tabInfo);
                            console.log(tabInfo.url + ' ok');
                            console.log(tabService.tabs);
                            $rootScope.$emit("tabs.update", tabInfo);
                        });
                    /*});*/
                    
                }else{
                    console.log("URL existente en array: " + tab.url); return;
                }

            }
        });
    }
  
    return tabService;
}