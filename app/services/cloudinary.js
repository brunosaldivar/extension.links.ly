'use strict';
App.Services.Cloudinary = function($q){

    var service = {};

    service.upload = function(img, tabInfo)
    {
      return new $q(function(resolve, reject){
        var url = 'https://api.cloudinary.com/v1_1/linksly/upload';
        var xhr = new XMLHttpRequest();
        var fd = new FormData();

        fd.append('upload_preset', 'linksly');
        fd.append('tags', tabInfo.tags);    
        fd.append('file', img);
        xhr.open('POST', url, true);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

        xhr.onreadystatechange = function(e) {
            if (xhr.readyState == 4){
                if( xhr.status == 200) {
                    var response = JSON.parse(xhr.responseText);
                    tabInfo.img = response.secure_url
                    resolve(tabInfo);
                }else{
                    tabInfo.img = xhr.status;
                    resolve(tabInfo);
                }
            }    
        };
        xhr.send(fd);
        img = null;
        fd = null;
      });
    }

    return service;
}