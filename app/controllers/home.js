App.Controllers.Home = function(SocketService, AuthenticationService, TabsService ,$scope, $rootScope, $http, $timeout, $q)
{    
      var vm = this;

    $scope.tabs = [];
    $scope.lastIds = [];

    $scope.init = function(){
       TabsService.getAll({}, function(d){
          $scope.tabs.push.apply($scope.tabs,d);
          lastIds.push( $scope.tabs[$scope.tabs.length -1]._id);
        });
    };
     /*  TabsService.getAll({}, function(d){
                  self.loadedPages[pageNumber].push.apply(self.loadedPages[pageNumber],d);
                });
         TabsService.getAll(function(d){
                $scope.tabs = d;
            });
     */   
  
    $scope.getShortString = function(str, ct){
        if(str && str.length > ct){
            str = str.substring(0,ct) + " ...";
        }
        return str;
    }
    $scope.getFecha = function(f){
        const monthNames = [" Jan. ", " Feb. ", " Mar. ", " Apr. ", " May. ", " Jun. ",
            " Jul. ",  " Aug. ", " Sep. ", " Oct.", " Nov. ", " Dec. "];
        date = new Date(f);
        year = date.getFullYear();
        month = monthNames[date.getMonth()];
        dt = date.getDate();
        return dt + month + year;
    }
    $scope.username = App.Security.mail;
    $scope.skip = 0;
    $scope.count = 0;  
    $scope.dataSource = {
        minIndex: 1,
        maxIndex: 10,
        getCount : function(){
            var p = $q.defer();
            if($scope.count == 0 )
            {
                TabsService.getAllCount().then(function(d){
                    console.log('count: '  +d)
                    $scope.count = d;
                    
                    p.resolve();
                });
            }else{

                p.resolve();
            }
            return p.promise;
        },
        get : function(index, count, success){

            console.log('requested index = ' + index + ', count = ' + count);

            var result = [];
            
            this.getCount().then(angular.bind(this,function(){
                var start = Math.max(1,index);
                var end = Math.min(index + count -1, $scope.count);
                console.log('start: ' + start + ' end: ' + $scope.count);
                if (start <= end) {

                    if(start == 1 && $scope.tabs.length === 0 ){
                        var opt ={
                            limit: 10,
                        };
                        TabsService.getAll(opt).then(function(d){

                            $scope.tabs.push.apply($scope.tabs,d);     
                            var lastId=$scope.tabs[$scope.tabs.length -1]._id;
                            $scope.lastIds.push( lastId);    
                            //parte desde start hasta end +1 onda skip
                        // result = $scope.tabs.slice(start, end + 1);
                            success(d);
                        });
                    }else{
                        if($scope.lastIds.length > 0){

                            var lastId=$scope.lastIds[$scope.lastIds.length -1];
                            //$scope.lastIds.push( lastId);    

                            console.log($scope.lastIds);
                            console.log(lastId);

                            var opt = {
                                last_id :lastId,
                                limit: 10,
                                skip: 0,
                            };
                            
                            $scope.skip += opt.limit;
                            opt.skip = $scope.skip

                            TabsService.getAll(opt).then(function(d) {
                                $scope.tabs.push.apply($scope.tabs,d);
                                var lastId=$scope.tabs[$scope.tabs.length -1]._id;
                                $scope.lastIds.push( lastId);    
                                console.log($scope.tabs);
                                success(d);
                            });

                        }

                    }
                }else{ success([]);}
            }));
            
        },   
    };
    
    
}
   // $("a").each(function (index, element) {     this.href = this.href.replace('.html','.amp.html' );      } );

   