App.Controllers.Home2 = function(SocketService, AuthenticationService, TabsService ,$scope, $rootScope, $http, $timeout, $q, util)
{  
    $scope.skip = 0;
 
    $rootScope.$on('tabs.update', function (event, data) {
        console.log(data); 
        var tab = data;

        tab.shortTitle = util.getShortString(tab.title,71);
        tab.shortUrl =util.getShortString(tab.url,100);
        tab.shortDate = util.getDate(tab.dateCreated);
        $scope.dynamicItems.addItem(tab);
    });
    
    TabsService.getTabInfo();


    $scope.loading= false;
    var DynamicItems = function() {
        /**
         * @type {!Object<?Array>} Data pages, keyed by page number (0-index).
         */
        this.loadedPages = {};

        /** @type {number} Total number of items. */
        this.numItems = 0;

        /** @const {number} Number of items to fetch per request. */
        this.PAGE_SIZE = 50;

        this.fetchNumItems_();
        
        
    };
    DynamicItems.prototype.addItem = function(item){

        if( this.loadedPages[0]){
            this.loadedPages[0].unshift(item);
        }
    }
    // Required.
    DynamicItems.prototype.getItemAtIndex = function(index) {

      //  console.log(index)
        var pageNumber = Math.floor(index / this.PAGE_SIZE);
        var page = this.loadedPages[pageNumber];

        if (page) {
        return page[index % this.PAGE_SIZE];
        } else if (page !== null) {
        this.fetchPage_(pageNumber);
        }
    };

    // Required.
    DynamicItems.prototype.getLength = function() {
        //console.log('holanda: ' +  this.numItems)
        return this.numItems;
    };

    DynamicItems.prototype.fetchPage_ = function(pageNumber) {
        // Set the page to null so we know it is already being fetched.
        this.loadedPages[pageNumber] = null;

        // For demo purposes, we simulate loading more items with a timed
        // promise. In real code, this function would likely contain an
        // $http request.
        this.loadedPages[pageNumber] = [];
        var pageOffset = pageNumber * this.PAGE_SIZE;
        var opt = {
            limit: this.PAGE_SIZE,
            skip: 0,
        };
        $scope.skip += opt.limit;
        opt.skip = pageOffset; //$scope.skip
        $scope.loading = true;
        TabsService.getAll(opt).then(angular.bind(this,function(d){
            for (var i = 0; i < d.length ; i++) {
                
                var tab = d[i];
                if(!tab) continue;
                tab.shortTitle = util.getShortString(tab.title,71);
                tab.shortUrl =util.getShortString(tab.url,100);
                tab.shortDate = util.getDate(tab.dateCreated);
                tab.checked = false;
                this.loadedPages[pageNumber].push(tab);
                
                //this.loadedPages[pageNumber].push.apply(this.loadedPages[pageNumber],d);
                $scope.loading = false;
            }
        }));
    };

    DynamicItems.prototype.fetchNumItems_ = function() {
        // For demo purposes, we simulate loading the item count with a timed
        // promise. In real code, this function would likely contain an
        // $http request.

        TabsService.getAllCount().then(angular.bind(this,function(d){
            console.log('count: '  +d)
            this.numItems = d;
        }));
    };

    $scope.dynamicItems = new DynamicItems();
    //db.urls.remove( {'title' : ''})
}