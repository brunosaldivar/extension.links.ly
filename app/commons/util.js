App.Common.Utils = function()
{
	var utils = {};
	
  const monthNames = [" Jan. ", " Feb. ", " Mar. ", " Apr. ", " May. ", " Jun. ",
            " Jul. ",  " Aug. ", " Sep. ", " Oct.", " Nov. ", " Dec. "];
  utils.getDate = function(f){
      if(f)
          date = new Date(f);
      else
          date = new Date();

      year = date.getFullYear();
      month = monthNames[date.getMonth()];
      dt = date.getDate();
      var min =  date.getMinutes().toString();
      if(min.length == 1)
        min = '0' + min;
      return dt + month + year + ' (' + date.getHours() + ':' + min + ')';// ':' + date.getSeconds()+')';
  }
  utils.getShortString = function(str, ct){
        if(str && str.length > ct){
            str = str.substring(0,ct) + " ...";
        }
        return str;
    }
  const traceFn = (fn, context) => function () {
    console.trace(`${fn.name} called with arguments: `, arguments);
    return fn.apply(context || this, arguments);
  }

  utils.getTags = function (title) {
      var tags = [];
      var temp = title.split(' ');

      for (var i = 0; i < temp.length; i++) { 
        if(temp[i].length > 3){
          if(tags[temp[i]] === undefined)
          {
            tags.push(temp[i]);
          }
        }
      }
      return tags;
    }
      
  /*

      traceProperty(window, '_');
      window._ = 3;
      VM2109:9 setting _ to  3

  */

  const traceProperty = (object, property) => {
    let value = object[property];
    Object.defineProperty(object, property, {
      get () {
        console.trace(`${property} requested`);
        return value;
      },
      set (newValue) {
        console.trace(`setting ${property} to `, newValue);
        value = newValue;
      },
    })
  };

  return utils;
}